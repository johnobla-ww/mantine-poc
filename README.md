# Mantine Proof of Concept

## Running the App

**Live Site:** 🚀 https://johnobla-ww.gitlab.io/mantine-poc/ 🚀 \
**Local Site:**

- Run: `yarn && yarn nx serve demo-shell`
- Go to http://localhost:4200/

<hr>

## Case Study

### Intro

This project is a test to see if we can use Mantine to power our design system.\
The test was a massive success.

### Why Mantine?

_Mantine Homepage_ - https://mantine.dev/

Mantine has benefits like:

- First class theming support
- Beautiful and extensive component library
- Style functions already included (I don't need to write or maintain this anymore)
- Superb documentation
- Completely free and open source

### How was this tested?

I chose the results page from `retirement-options-guidance` to replicate using Mantine, while providing an option to swap between tenants.

`Mantine` version - https://johnobla-ww.gitlab.io/mantine-poc/<br />
`Existing` version - https://ww-green.wealthwizards.io/retirement-options-guidance-shell/results

### Conclusion

Mantine is the perfect tool for us to use. It combines complete extensibility with smart defaults, and will allow us to scale our design system without forcing us to rewrite everything from scratch.

<hr>

## Project Details

### App Shell

- The main app logic is in `apps/demo-shell/src/app/app.tsx`
- This is a shell containing copy, theme, and a ThemeWrapper that injects styling using `React Context`
- Every other component is imported from a shared libary

### Shared Library

- The shared libary is in `libs/core/components`
- This contains every component needed for the app to run
- All components have sensibile default
  - Most of the time you don't need to add new props
- All components have the option to be extended using Mantine's API
