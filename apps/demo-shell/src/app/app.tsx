import { useState } from 'react';
import {
  Group,
  Center,
  MantineProvider,
  MantineProviderProps,
} from '@mantine/core';
import {
  Button,
  ButtonBlock,
  CallToActionGroup,
  IntroHeadings,
  SplitContainer,
  TenantPicker,
} from '@mantine-poc/core/components';

const theme = {
  fontFamily: 'Nunito Sans, Verdana, sans-serif',
  headings: { fontFamily: 'Nunito Sans, Verdana, sans-serif' },
};

const styles: MantineProviderProps['styles'] = {
  Button: ({ colors, primaryColor, white }) => ({
    root: {
      backgroundColor: colors[primaryColor],
    },
    outline: {
      border: `solid ${white} 1px`,
      color: white,
    },
  }),
};

const copy = {
  backButton: 'Back',
  mainTitle: 'Your retirement income options',
  subTitle:
    "We've used the information you provided to create a report showing your retirement income options. Please note, this report is for information purposes only and does not represent a personal reccomendation.",
  downloadText: 'Your report is ready for you to download',
  downloadButton: 'Download Now',
  ctaTitle: 'If you need help or advice, please contact our team',
  ctaData: [
    {
      title: 'Book an appointment',
      bodyText: 'You can schedule a date and time that suits you',
    },
    {
      title: 'Chat to us online',
      bodyText: 'Alternatively, you can chat to us right now',
    },
  ],
};

export function App() {
  const [themeColor, setThemeColor] = useState('violet');

  return (
    <MantineProvider
      theme={{ ...theme, primaryColor: themeColor }}
      styles={styles}
    >
      <SplitContainer>
        <Group grow direction="column">
          <Group>
            <Button>{copy.backButton}</Button>
          </Group>
          <IntroHeadings title={copy.mainTitle} subtitle={copy.subTitle} />
          <ButtonBlock
            bodyText={copy.downloadText}
            buttonText={copy.downloadButton}
          />
          <CallToActionGroup title={copy.ctaTitle} data={copy.ctaData} />
        </Group>
        <Center>
          <Group direction="column">
            <TenantPicker
              themeColor={themeColor}
              setThemeColor={setThemeColor}
            />
          </Group>
        </Center>
      </SplitContainer>
    </MantineProvider>
  );
}

export default App;
