import { useMantineTheme, MantineSize } from '@mantine/core';
import { useMediaQuery as useMantineMediaQuery } from '@mantine/hooks';

export const useMediaQuery = (breakpoint: MantineSize) => {
  const theme = useMantineTheme();
  const isBelowBP = useMantineMediaQuery(
    `(max-width: ${theme.breakpoints[breakpoint]}px)`
  );

  return isBelowBP;
};

export default useMediaQuery;
