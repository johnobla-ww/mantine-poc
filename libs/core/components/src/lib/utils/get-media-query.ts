export const getMediaQuery = (breakpoint: number) =>
  `@media (min-width: ${breakpoint}px)`;

export default getMediaQuery
