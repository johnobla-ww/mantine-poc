import { createStyles, Group } from '@mantine/core';
import { ReactNode } from 'react';
import getMediaQuery from '../utils/get-media-query';
import useMediaQuery from '../utils/useMediaQuery';

export interface SplitContainerProps {
  children: ReactNode;
}

export const SplitContainer = ({ children }: SplitContainerProps) => {
  const { classes } = useStyles();
  const isBelowMQ = useMediaQuery('md');

  return (
    <Group
      grow
      direction={isBelowMQ ? 'column' : 'row'}
      className={classes.root}
    >
      {children}
    </Group>
  );
};

const useStyles = createStyles(({ spacing, breakpoints }) => ({
  root: {
    [getMediaQuery(breakpoints.sm)]: {
      flexDirection: 'row',
      padding: `0 ${spacing.xl}px`,
      maxWidth: 'none',
    },
  },
}));
