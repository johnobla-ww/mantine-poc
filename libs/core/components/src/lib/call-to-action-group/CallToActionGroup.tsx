import { createStyles, Group, Title } from '@mantine/core';
import {
  CallToAction,
  CallToActionProps,
} from '../call-to-action/CallToAction';
import useMediaQuery from '../utils/useMediaQuery';

export interface CallToActionGroupProps {
  title: string;
  data: CallToActionProps[];
}

export const CallToActionGroup = ({
  title,
  data,
  ...props
}: CallToActionGroupProps) => {
  const { classes } = useStyles();
  const isBelowMQ = useMediaQuery('xs');

  return (
    <Group {...props} direction="column" grow>
      <Group className={classes.titleContainer}>
        <Title className={classes.title} order={2}>
          {title}
        </Title>
      </Group>
      <Group grow noWrap={!isBelowMQ} direction={isBelowMQ ? 'column' : 'row'}>
        {data.map(({ title, bodyText, buttonText }) => (
          <CallToAction
            key={title}
            title={title}
            bodyText={bodyText}
            buttonText={buttonText}
          />
        ))}
      </Group>
    </Group>
  );
};

const useStyles = createStyles(({ fontSizes, spacing }) => ({
  titleContainer: { paddingTop: spacing.xl },
  title: {
    fontSize: fontSizes.lg,
    fontWeight: 800,
    padding: `${spacing.xl} 0`,
  },
}));

