import { createStyles, Group, Text } from '@mantine/core';
import { Button } from '../button/Button';
import getMediaQuery from '../utils/get-media-query';

export interface ButtonBlockProps {
  bodyText: string;
  buttonText: string;
}

export const ButtonBlock = ({
  bodyText,
  buttonText,
  ...props
}: ButtonBlockProps) => {
  const { classes } = useStyles();

  return (
    <Group {...props} className={classes.root}>
      <Text color="white" weight={700} className={classes.text}>
        Your report is ready for you to download
      </Text>
      <Button className={classes.button} size="lg" variant="outline">
        Download Now
      </Button>
    </Group>
  );
};

const useStyles = createStyles(
  ({ spacing, colors, primaryColor, fontSizes, breakpoints }) => ({
    root: {
      backgroundColor: colors[primaryColor],
      padding: `${spacing.md}px ${spacing.xl}px`,

      flexDirection: 'column',
      justifyContent: 'center',

      [getMediaQuery(breakpoints.sm)]: {
        flexDirection: 'row',
        justifyContent: 'space-between',
      },

      [getMediaQuery(breakpoints.md)]: {
        flexDirection: 'column',
        justifyContent: 'center',
      },

      [getMediaQuery(breakpoints.lg)]: {
        flexDirection: 'row',
        justifyContent: 'space-between',
      },
    },

    text: {
      textAlign: 'center',

      [getMediaQuery(breakpoints.xl)]: {
        textAlign: 'start',
      },
    },

    button: {
      fontSize: fontSizes.md,
      fontWeight: 700,
    },
  })
);
