import { Dispatch, SetStateAction } from 'react';
import { Radio, RadioGroup } from '@mantine/core';

export interface TenantPickerProps {
  themeColor: string;
  setThemeColor: Dispatch<SetStateAction<string>>;
}

export const TenantPicker = ({
  themeColor,
  setThemeColor,
}: TenantPickerProps) => {
  return (
    <RadioGroup
      size="lg"
      label="Choose your tenant"
      value={themeColor}
      onChange={setThemeColor}
      required
    >
      <Radio value="violet">1</Radio>
      <Radio value="red">2</Radio>
      <Radio value="pink">3</Radio>
      <Radio value="grape">4</Radio>
      <Radio value="indigo">5</Radio>
      <Radio value="blue">6</Radio>
      <Radio value="cyan">7</Radio>
      <Radio value="teal">8</Radio>
      <Radio value="green">9</Radio>
      <Radio value="lime">10</Radio>
      <Radio value="yellow">11</Radio>
      <Radio value="orange">12</Radio>
      <Radio value="gray">13</Radio>
    </RadioGroup>
  );
};
