import { createStyles, Group, Text, Title } from '@mantine/core';

export interface IntroHeadingsProps {
  title: string;
  subtitle: string;
}

export const IntroHeadings = ({
  title,
  subtitle,
  ...props
}: IntroHeadingsProps) => {
  const { classes } = useStyles();

  return (
    <Group {...props} direction="column">
      <Title className={classes.title}>{title}</Title>
      <Text color="gray">{subtitle}</Text>
    </Group>
  );
};

const useStyles = createStyles(({ fontSizes }) => ({
  title: {
    fontSize: fontSizes.xl,
    fontWeight: 800,
  },
}));
