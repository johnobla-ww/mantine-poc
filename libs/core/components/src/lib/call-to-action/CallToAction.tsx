import { createStyles, Group, Text, Title } from '@mantine/core';
import { Button } from '../button/Button';
import getMediaQuery from '../utils/get-media-query';

export interface CallToActionProps {
  title: string;
  bodyText: string;
  buttonText?: string;
}

export const CallToAction = ({
  title,
  bodyText,
  buttonText = 'Book Now',
  ...props
}: CallToActionProps) => {
  const { classes } = useStyles();

  return (
    <Group {...props} className={classes.root} direction="column">
      <Group direction="column" position="apart">
        <Title className={classes.title} order={3}>
          {title}
        </Title>
        <Text color="gray">{bodyText}</Text>
      </Group>
      <Button className={classes.button} size="lg">
        {buttonText}
      </Button>
    </Group>
  );
};

const useStyles = createStyles(
  ({ colors, primaryColor, spacing, fontSizes, breakpoints }) => ({
    root: {
      width: '100%',
      justifyContent: 'space-between',
      border: `0.5px solid ${colors.gray[5]}`,
      padding: `${spacing.lg}px ${spacing.md}px`,

      [getMediaQuery(breakpoints.xs)]: {
        height: '18rem',
      },
    },
    title: {
      color: colors[primaryColor],
      fontSize: fontSizes.xl,
    },
    button: {
      width: '100%',
    },
  })
);
